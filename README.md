# README #

This repo contains a set of standalone, generic ROS applications and scripting tools intended for reference and reuse. Most nodes and tools will be lightweight. 


### Contribution guidelines ###

When adding a tool or node to this repo, consider the following: 

- Nodes / tools must be descriptively named, to the point where another engineer could tell the purpose based on the name. When this is not possible a README *must* be included.
- All ROS nodes must include build files, because who the hell wants to write a CMake file? 
