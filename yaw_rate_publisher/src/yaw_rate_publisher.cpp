#include "ros/ros.h"
#include "tf2_geometry_msgs/tf2_geometry_msgs.h"
#include "geometry_msgs/TwistStamped.h"
#include "geometry_msgs/TwistWithCovarianceStamped.h"

using namespace std;


ros::Publisher twist_pub;
ros::Timer publish_empty_twist;
geometry_msgs::TwistWithCovarianceStamped twistMsg; 
geometry_msgs::Twist emptyTwistMsg; 

void publishEmptyTwist(const ros::TimerEvent& e)
{
  twistMsg.twist.twist = emptyTwistMsg;
  twistMsg.header.stamp = e.current_real;
  twist_pub.publish(twistMsg);
}

// Process incoming geometry message to extract the yaw rate, and publish a message
// containing the ego-vehicle yaw rate
void twistCallback(const geometry_msgs::TwistStampedConstPtr & msg)
{
    publish_empty_twist.setPeriod(ros::Duration(0.1), true);
    
    twistMsg.header.stamp = ros::Time::now();
    twistMsg.twist.twist = msg->twist; 
    
    twist_pub.publish(twistMsg);
}

// Create a node that will publish a geometry message containing the ego-vehicle yaw rate
// Subscribe to a geometry message and register a callback function
int main(int argc, char **argv)
{
  ros::init(argc, argv, "yaw_rate_publisher");

  ros::NodeHandle n;

  twist_pub = n.advertise<geometry_msgs::TwistWithCovarianceStamped>("ego_vehicle_yaw_rate", 1000);

  ros::Subscriber sub = n.subscribe("source_raw_rate", 1000, twistCallback);
  
  publish_empty_twist = n.createTimer(ros::Duration(0.1), publishEmptyTwist);

  ros::spin();

  return 0;
}
