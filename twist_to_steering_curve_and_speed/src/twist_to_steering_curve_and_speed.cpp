/*
* AutonomouStuff, LLC ("COMPANY") CONFIDENTIAL
* Unpublished Copyright (c) 2009-2017 AutonomouStuff, LLC, All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains the property of COMPANY. The intellectual and technical concepts contained
* herein are proprietary to COMPANY and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
* from COMPANY.  Access to the source code contained herein is hereby forbidden to anyone except current COMPANY employees, managers or contractors who have executed
* Confidentiality and Non-disclosure agreements explicitly covering such access.
*
* The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
* information that is confidential and/or proprietary, and is a trade secret, of  COMPANY.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
* OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
* LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
* TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
*/

#include "ros/ros.h"

#include <std_msgs/Bool.h>
#include <sensor_msgs/Imu.h>
#include <module_comm_msgs/SpeedMode.h>
#include <module_comm_msgs/SteerMode.h>
#include <module_comm_msgs/VelocityAccel.h>
#include <platform_comm_msgs/SteeringFeedback.h>
#include <platform_comm_msgs/ThrottleFeedback.h>
#include <platform_comm_msgs/GearFeedback.h>
#include <platform_comm_msgs/HillStartAssist.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Vector3Stamped.h>
#include <dbw_mkz_msgs/SteeringReport.h>
#include <dbw_mkz_msgs/ThrottleReport.h>
#include <dbw_mkz_msgs/GearReport.h>
#include <dbw_mkz_msgs/BrakeInfoReport.h>
#include <math.h>

using namespace std;

uint16_t engaged = 0;
uint16_t hsa_status = 0;
uint16_t gear = 1;
double pedal_fdbk = 0.0;
double steering_wheel_angle = 0.0;
double vel = 0.0;
double accel = 0.0;
double desired_speed = 0.0;
double desired_curvature = 0.0;
// Load from JSON?
double acceleration_limit = 2.0;
double deceleration_limit = 2.5;
double max_curvature_rate = 0.1;

// Callback that handles cmd_vel topic messages
void commandVelCallback(const geometry_msgs::Twist::ConstPtr& msg)
{
  desired_speed = msg->linear.x;
  double yaw_rate = msg->angular.z;
  desired_curvature = yaw_rate / desired_speed;
}

// Callback that reroutes steering report info
void steerReportCallback(const dbw_mkz_msgs::SteeringReport::ConstPtr& msg)
{
  steering_wheel_angle = msg->steering_wheel_angle;
}

// Callback to handle throttle report
void throttleReportCallback(const dbw_mkz_msgs::ThrottleReport::ConstPtr& msg)
{
  pedal_fdbk = msg->pedal_output;
}

// Callback to handle gear report
void gearReportCallback(const dbw_mkz_msgs::GearReport::ConstPtr& msg)
{
  gear = msg->state.gear;
}

// Callback that handles brake report messages in regards to hill start assist
void brakeInfoReportCallback(const dbw_mkz_msgs::BrakeInfoReport::ConstPtr& msg)
{
  hsa_status = msg->hsa.status;
}

// Callback that handles dbw enable/disable messages
void dbwEnabledCallback(const std_msgs::Bool::ConstPtr& msg)
{
  if(msg->data)
  {
    engaged = 1;
  }
  else
  {
    engaged = 0;
  }
}

// Callback to handle sim GPS data for parsing velocity
// Calculates resultant of x and y components
void gpsVelCallback(const geometry_msgs::Vector3Stamped::ConstPtr& msg)
{
  double x_comp = msg->vector.x;
  double y_comp = msg->vector.y;
  vel = sqrt((x_comp * x_comp) + (y_comp * y_comp));
}

// Callback to handle sim GPS data for parsing acceleration
void gpsAccelCallback(const sensor_msgs::Imu::ConstPtr& msg)
{
  // Resultant of x and y accel?
  // Neglect z ~ 9.8 for gravity
  accel = msg->linear_acceleration.y;
}

// Main routine
int main(int argc, char **argv)
{
  double publish_interval = 0.05;

  // ROS initialization
  ros::init(argc, argv, "dbw_mkz_translator");
  ros::NodeHandle n;
  ros::Rate loop_rate(1.0 / publish_interval);

  // Advertise messages to send
  ros::Publisher speed_pub = n.advertise<module_comm_msgs::SpeedMode>("arbitrated_speed_commands", 1);
  ros::Publisher steer_pub = n.advertise<module_comm_msgs::SteerMode>("arbitrated_steering_commands", 1);
  ros::Publisher steer_fdbk_pub = n.advertise<platform_comm_msgs::SteeringFeedback>("steering_feedback", 1);
  ros::Publisher throttle_fdbk_pub = n.advertise<platform_comm_msgs::ThrottleFeedback>("throttle_feedback", 1);
  ros::Publisher gear_fdbk_pub = n.advertise<platform_comm_msgs::GearFeedback>("gear_feedback", 1);
  ros::Publisher hill_climb_pub = n.advertise<platform_comm_msgs::HillStartAssist>("hill_start_assist", 1);
  ros::Publisher vel_accel_pub = n.advertise<module_comm_msgs::VelocityAccel>("velocity_accel", 1);

  // Subscribe to messages to read
  ros::Subscriber velocity_sub = n.subscribe("cmd_vel",
                                             1,
                                             commandVelCallback);
  ros::Subscriber steering_sub = n.subscribe("steering_report",
                                             1,
                                             steerReportCallback);
  ros::Subscriber throttle_sub = n.subscribe("throttle_report",
                                             1,
                                             throttleReportCallback);
  ros::Subscriber gear_sub = n.subscribe("gear_report",
                                         1,
                                         gearReportCallback);
  ros::Subscriber brake_info_sub = n.subscribe("brake_info_report",
                                               1,
                                               brakeInfoReportCallback);
  ros::Subscriber dbw_enable_sub = n.subscribe("dbw_enabled",
                                               1,
                                               dbwEnabledCallback);
  ros::Subscriber gps_vel_sub = n.subscribe("perfect_gps/vel",
                                            1,
                                            gpsVelCallback);
  ros::Subscriber gps_accel_sub = n.subscribe("imu/data_raw",
                                              1,
                                              gpsAccelCallback);

  // Wait for time to be valid
  while (ros::Time::now().nsec == 0);

  // Loop as long as module should run
  while (ros::ok())
  {
    // Get current time
    ros::Time now = ros::Time::now();

    // Velocity and acceleration message
    module_comm_msgs::VelocityAccel vel_accel_msg;
    vel_accel_msg.header.stamp = now;
    vel_accel_msg.velocity = vel;
    vel_accel_msg.accleration = accel;
    vel_accel_pub.publish(vel_accel_msg);

    // Hill start assist message
    platform_comm_msgs::HillStartAssist hill_start_msg;
    hill_start_msg.header.stamp = now;
    hill_start_msg.active = hsa_status;
    hill_climb_pub.publish(hill_start_msg);

    // Gear feedback message
    platform_comm_msgs::GearFeedback gear_fdbk_msg;
    gear_fdbk_msg.header.stamp = now;
    gear_fdbk_msg.current_gear.gear = gear;
    gear_fdbk_pub.publish(gear_fdbk_msg);

    // Pedal feedback message
    platform_comm_msgs::ThrottleFeedback throttle_fdbk_msg;
    throttle_fdbk_msg.header.stamp = now;
    throttle_fdbk_msg.throttle_pedal = pedal_fdbk;
    throttle_fdbk_pub.publish(throttle_fdbk_msg);

    // Steering feedback message
    platform_comm_msgs::SteeringFeedback steer_fdbk_msg;
    steer_fdbk_msg.header.stamp = now;
    steer_fdbk_msg.steering_wheel_angle = steering_wheel_angle;
    steer_fdbk_pub.publish(steer_fdbk_msg);

    // Arbitrated speed messages
    module_comm_msgs::SpeedMode speed_msg;
    speed_msg.header.stamp = now;
    speed_msg.mode = engaged;
    speed_msg.speed = desired_speed;
    speed_msg.acceleration_limit = acceleration_limit;
    speed_msg.deceleration_limit = deceleration_limit;
    speed_pub.publish(speed_msg);

    // Arbitrated steering messages
    module_comm_msgs::SteerMode steer_msg;
    steer_msg.header.stamp = now;
    steer_msg.mode = engaged;
    steer_msg.curvature = desired_curvature;
    steer_msg.max_curvature_rate = max_curvature_rate;
    steer_pub.publish(steer_msg);

    // Wait for next loop
    loop_rate.sleep();

    // Receive messages
    ros::spinOnce();
  }
}
