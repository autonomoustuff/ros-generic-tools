# Twist to Steering Curve and Speed module

### Launching
In order to launch the DS simulator, first copy the "as_dbw.launch" file to
/opt/ros/kinetic/share/dbw_mkz_can/launch/.

Next, run the provided "as_lane_keep_demo.launch" file.
